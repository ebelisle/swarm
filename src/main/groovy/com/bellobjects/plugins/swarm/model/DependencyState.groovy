package com.bellobjects.plugins.swarm.model

enum DependencyState {
    DEPENDENCY, REMOTE, UNCLONED, DIRECTORY, PROJECT
}
