package com.bellobjects.plugins.swarm.model

import org.gradle.api.Project;
import org.gradle.api.artifacts.Dependency;

class SwarmNode {
    SwarmProject project
    List<SwarmNode> subnodes = []
    DependencyState state = DependencyState.DEPENDENCY

    public SwarmNode(SwarmProject project) {
        this.project = project
    }
    
    public SwarmNode(Map map) {
        project = map.project
        subnodes = map.subnodes.collect{
            new SwarmNode(it)
        }
    }
    
    String toString() {
        "${state.toString().padRight(11)} ${project}  "
    }
    
}
