package com.bellobjects.plugins.swarm.model

import java.util.Map;

import groovy.json.JsonOutput;
import groovy.json.JsonSlurper

import org.gradle.api.GradleException
import org.gradle.api.Project;

class SwarmSettings {
    SwarmNode root
    boolean linked
    List<String> buildOrder
    List<String> includes
    
    public SwarmSettings() {
        
    }
    
    public SwarmSettings(Map map) {
        root = new SwarmNode(map.root)
        linked = map.linked
        buildOrder = map.buildOrder
        includes = map.includes
    }
    
    SwarmSettings link(boolean linked) {
        this.linked = linked
        this
    }

    SwarmSettings save(Project project) {
        project.file(".swarm.json").write(JsonOutput.toJson(this) + "\n")
        this
    }
    
    static SwarmSettings load(Project project) {
        File file = project.file(".swarm.json")
        if (!file.exists()) {
            throw new GradleException("Project not initialized.  Run task swarmInit")
        }
        new JsonSlurper().parse(file) as SwarmSettings
    }
    
    SwarmSettings printStatus() {
        println("\nDEPENDENCY TREE\n----")
        printNode("", root)
        println("linked: ${linked}")
        println("buildOrder: ${buildOrder}")
        println("includes: ${includes}")
        this
    }
    
    private static void printNode(String indent, SwarmNode node) {
        println(indent + node.project.name)
        node.subnodes.each {
            printNode("${indent}  ", it)
        }
    }
    
    void initilizationPhase() {
        println("*********InitilizationPhase************")
    }

}
