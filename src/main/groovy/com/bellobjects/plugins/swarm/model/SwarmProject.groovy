package com.bellobjects.plugins.swarm.model

import org.gradle.api.Project;
import org.gradle.api.artifacts.Dependency;

class SwarmProject {
    String group;
    String name;
    String version;
    
    SwarmProject() {
        
    }
    
    SwarmProject(Dependency dependency) {
        group = dependency.group
        name = dependency.name
        version = dependency.version
    }
    
    SwarmProject(Project project) {
        group = project.group
        name = project.name
        version = project.version
    }
    
    String toString() {
        return "${group}:${name}:${version}"
    }
}
