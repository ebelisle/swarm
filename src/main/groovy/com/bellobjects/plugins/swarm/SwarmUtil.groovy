package com.bellobjects.plugins.swarm

import groovy.json.JsonSlurper;

import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.ajoberstar.grgit.Grgit;
import org.ajoberstar.grgit.Remote
import org.eclipse.jgit.lib.Repository
import org.eclipse.jgit.lib.RepositoryBuilder
import org.eclipse.jgit.lib.StoredConfig
import org.gradle.api.DomainObjectCollection;
import org.gradle.api.DomainObjectSet;
import org.gradle.api.GradleException
import org.gradle.api.Project;
import org.gradle.api.Task;
import org.gradle.api.artifacts.Configuration;
import org.gradle.api.artifacts.Dependency;
import org.gradle.tooling.GradleConnector;
import org.gradle.tooling.ModelBuilder;
import org.gradle.tooling.ProjectConnection;
import org.gradle.tooling.model.GradleProject;
import org.gradle.tooling.model.GradleTask;
import org.gradle.tooling.model.build.BuildEnvironment;
import org.slf4j.Logger
import org.slf4j.LoggerFactory;

import com.bellobjects.plugins.swarm.model.DependencyState;
import com.bellobjects.plugins.swarm.model.SwarmNode;
import com.bellobjects.plugins.swarm.model.SwarmProject;

class SwarmUtil {
    private static Logger logger = LoggerFactory.getLogger(this);

    static SwarmNode buildTree(Project project) {
        SwarmNode root = new SwarmNode(new SwarmProject(project));

        // First generation nodes are known by project
        root.subnodes = getLocalDependencies(project).collect{new SwarmNode(it)}
        
        // Second and deeper generations require querying remote project
        root.subnodes.each{addRemoteDependencies(project, it)}
        root  
    }

    static Remote getRemoteOrigin(project) {
        def grgit = org.ajoberstar.grgit.Grgit.open(dir: project.rootDir);
        def remotes = grgit.remote.list()
        def origin = remotes.find {it.name == 'origin'}
        if (origin == null) {
            throw new GradleException("Remote 'origin' not found in root project");
        }
        origin
    }
        
    static void checkoutSubproject(Project project, SwarmNode node) {
        Remote origin = getRemoteOrigin(project)
        def newUri = origin.url.replace(project.name, node.project.name)
        println("**Cloning: ${newUri}")
        Grgit.clone(dir : "../${node.project.name}", uri: newUri)
    }
    
    static void addRemoteDependencies(Project project, SwarmNode node) {
        File projectDir = project.file("../${node.project.name}")
        if (!projectDir.exists()) {
            checkoutSubproject(project, node)
        }
        
        println("projectDir:  ${projectDir}")
        logger.debug("VisitChild: ${projectDir}")
        
        ProjectConnection connection = GradleConnector.newConnector().forProjectDirectory(projectDir).connect();
        
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        logger.debug("READING: ${projectDir}")

        try {
            connection.newBuild().forTasks("swarmDependencies").setStandardOutput(output).run()
        }
        catch (Exception e) {
            throw e;
        }
        
        finally {
            connection.close();
        }

        logger.debug("OUTPUT\n------\n" + output.toString() + "--")
        
        Matcher m = output.toString() =~ /(?m)(?s):swarmDependencies\n([^\n]*).*/
        if (m.matches()) {
            String json = m[0][1]
            if (json == null) {
                logger.error("Did not parse:\n--------------\n${output}\n--------------")
                return
            }
            def slurped = new JsonSlurper().parseText(json)
            node.subnodes = slurped.collect{
                new SwarmNode(new SwarmProject(it))
            }            
            node.subnodes.each{ addRemoteDependencies(project, it) }
        }
    }
    
    static List<SwarmProject> getLocalDependencies(Project project) {
        project.configurations
        .collect() {Configuration configuration ->
            configuration.dependencies
                .findAll{ Dependency dependency -> dependency.group == project.group }
                .collect{ Dependency dependency -> new SwarmProject(dependency) }
        }
        .flatten()
        .unique()
    }
    
    static List<String> buildOrder(SwarmNode swarmNode) {
        List<String> nodeNames = []
        buildOrder(swarmNode, nodeNames)
        nodeNames
    }
    
    static void buildOrder(SwarmNode swarmNode, List<String> nodeNames) {
        swarmNode.subnodes.each {
            buildOrder(it, nodeNames)
        }
        
        if (!nodeNames.contains(swarmNode.project.name)) {
            nodeNames.add(swarmNode.project.name)    
        }
    }
    
}
