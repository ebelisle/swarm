package com.bellobjects.plugins.swarm

import java.util.Map;

import org.gradle.api.DefaultTask;
import org.gradle.api.Project
import org.gradle.api.Plugin
import org.gradle.api.tasks.TaskAction;

import com.bellobjects.plugins.swarm.model.SwarmSettings;

import groovy.json.JsonOutput

class SwarmPlugin implements Plugin<Project> {
    void apply(Project project) {
        project.task('swarmDependencies', type: DependenciesTask, description: 'INTERNAL - project dependencies as json')   

        // root module only tasks
        if (project.parent == null) {
            project.task('swarmStatus', type: StatusTask, group: 'Swarm')
            project.task('swarmInit', type: InitTask, group: 'Swarm')
            project.task('swarmLink', type: LinkTask, group: 'Swarm')
            project.task('swarmUnlink', type: UnlinkTask, group: 'Swarm')
        }        
    }  
    
    static class InitTask extends DefaultTask {
        @TaskAction
        def initSwarm() {
            SwarmSettings s = new SwarmSettings()
            s.root = SwarmUtil.buildTree(project)
            s.buildOrder = SwarmUtil.buildOrder(s.root)
            s.includes = s.buildOrder.take(s.buildOrder.size() - 1)
            s.save(project)
        }
    }

    static class StatusTask extends DefaultTask {
        @TaskAction
        def status() {
            SwarmSettings.load(project).printStatus()
        }
    }
    
    static class LinkTask extends DefaultTask {
        @TaskAction
        def status() {
            SwarmSettings.load(project).link(true).save(project)
        }
    }
    
    static class UnlinkTask extends DefaultTask {
        @TaskAction
        def status() {
            SwarmSettings.load(project).link(false).save(project)
        }
    }

    
    static class DependenciesTask extends DefaultTask {
        @TaskAction
        def listDependencies() {
            println JsonOutput.toJson(SwarmUtil.getLocalDependencies(project))
        }
    }

}
