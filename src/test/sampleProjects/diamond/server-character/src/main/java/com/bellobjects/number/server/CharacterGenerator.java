package com.bellobjects.number.server;


public class CharacterGenerator {
    private final char seed;
    public CharacterGenerator(char seed) {
        this.seed = seed;
    }

    public char getCharacter() {
        return seed;
    }
}
