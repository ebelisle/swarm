package com.bellobjects.number.server;


public class LetterGenerator {
    private final CharacterGenerator characterGenerator;
    
    public LetterGenerator(char letter) {
        characterGenerator = new CharacterGenerator(letter);
    }

    public char getLetter() {
        return characterGenerator.getCharacter();
    }
}
