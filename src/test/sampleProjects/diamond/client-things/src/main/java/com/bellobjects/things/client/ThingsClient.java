package com.bellobjects.things.client;

import com.bellobjects.number.server.DigitGenerator;
import com.bellobjects.number.server.LetterGenerator;


public class ThingsClient {
    private final LetterGenerator letterGenerator = new LetterGenerator('A');
    private final DigitGenerator digitGenerator = new DigitGenerator('1');

    public ThingsClient() {
    }

    public void show() {
        System.out.println(letterGenerator.getLetter());
        System.out.println(digitGenerator.getDigit());
    }
}
