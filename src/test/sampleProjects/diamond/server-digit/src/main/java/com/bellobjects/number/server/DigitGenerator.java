package com.bellobjects.number.server;


public class DigitGenerator {
    private final CharacterGenerator characterGenerator;

    public DigitGenerator(char seed) {
        this.characterGenerator = new CharacterGenerator(seed);
    }

    public char getDigit() {
        return characterGenerator.getCharacter();
    }
}
