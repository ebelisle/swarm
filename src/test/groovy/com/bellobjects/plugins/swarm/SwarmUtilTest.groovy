package com.bellobjects.plugins.swarm;


import groovy.json.JsonSlurper

import org.junit.Test

import com.bellobjects.plugins.swarm.model.SwarmNode;
import com.bellobjects.plugins.swarm.model.SwarmProject;

import static org.junit.Assert.assertEquals
import static org.junit.Assert.assertTrue
import static org.junit.Assert.fail

class SwarmUtilTest {

    @Test
    public void test() {
        String json = '[{"group":"com.bellobjects.gradle","version":"0.+","name":"server-character"}]'
        def parsed = new JsonSlurper().parseText(json)
        List<SwarmProject> projects = parsed as List<SwarmProject>
        assertEquals(1, projects.size())
        SwarmProject swarmProject = projects.first()
        SwarmNode swarmNode = new SwarmNode(swarmProject)
        assertEquals("com.bellobjects.gradle", swarmNode.project.group)
        assertEquals("server-character", swarmNode.project.name)
    }
    
}
